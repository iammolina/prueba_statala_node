
var connection = require('../config/config');

var TicketMdl = {};


TicketMdl.getTicket = function(callback)
{
    connection.query("select * from ticket", function (error, results, fields) 
    {
        if (error)
        {
            return  callback(error , null);
        }else
        {
            return callback(null , results)
        }
    });
}


TicketMdl.getTicketID = function(id , callback)
{
    connection.query("select * from ticket where id_user = " +  id, function (error, results, fields) 
    {
        if (error)
        {
            return  callback(error , null);
        }else
        {
            return callback(null , results)
        }
    });
}






TicketMdl.postTicket = function(ticket ,callback)
{

    var post = { id_user : ticket.id_user , ticket_pedido : ticket.descripcion };

    connection.query('INSERT INTO ticket SET ?', post, function (error, results, fields)
    {
        if (error)
        {
            return  callback(error , null);
        }else
        {
            return callback(null , results)
        }
    });
}


TicketMdl.putTicket = function(ticket, callback)
{

    connection.query("UPDATE ticket SET  id_user = " + ticket.id_user + " ,  ticket_pedido = '" + ticket.ticket_pedido + "'  where id = " + ticket.id, function (error, results, fields)
    {
        if (error)
        {
            return  callback(error , null);
        }else
        {
            return callback(null , results)
        }
    });
}



TicketMdl.deleteTicket = function(id, callback)
{

    connection.query('DELETE from ticket where id = ' +  id, function (error, results, fields)
    {
        if (error)
        {
            return  callback(error , null);
        }else
        {
            return callback(null , results)
        }
    });
}



module.exports = TicketMdl;
