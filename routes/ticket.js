var express = require('express');
var TicketMdl = require('../models/ticketMdl');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) 
{
     TicketMdl.getTicket(function(error,data)
     {
        if(error)
         {
           res.status(500).json(error);
         }else
         {
    
           res.status(200).json(data);
    
         }
      })

});

router.get('/:id', function(req, res, next) 
{
     TicketMdl.getTicketID(req.params.id , function(error,data)
     {
        if(error)
         {
           res.status(500).json(error);
         }else
         {
    
           res.status(200).json(data);
    
         }
      })

});


/* GET users listing. */
router.post('/', function(req, res, next) 
{

    TicketMdl.postTicket( req.body , function(error,data)
    {
       if(error)
        {
          res.status(500).json(error);
        }else
        {
   
          res.status(200).json(data);
   
        }
     })

   
});

  
  /* GET users listing. */
router.put('/', function(req, res, next) 
{


    var ticket = { id : req.body.id ,  ticket_pedido : req.body.descripcion , id_user : req.body.id_user };
    
    TicketMdl.putTicket( ticket , function(error,data)
    {
       if(error)
        {
          res.status(500).json(error);
        }else
        {
   
          res.status(200).json(data);
   
        }
     })
});

  

  /* GET users listing. */
router.delete('/:id', function(req, res, next) 
{
    TicketMdl.deleteTicket( req.params.id, function(error,data)
    {
       if(error)
        {
          res.status(500).json(error);
        }else
        {
   
          res.status(200).json(data);
   
        }
     })

    

});
  
module.exports = router;
